const data = [
    { 
        place: "Melbourne", 
        country: "Australia", 
        location: {
            lat: '99',
            lng: '88'
        },
        temperature: '38 Degree Celsius'
    },{ 
        place: "New Delhi", 
        country: "India", 
        location: {
            lat: '84',
            lng: '44'
        },
        temperature: '42 Degree Celsius'
    },{ 
        place: "Pretoria", 
        country: "SouthAfrica", 
        location: {
            lat: '35',
            lng: '24'
        },
        temperature: '39 Degree Celsius'
    },{ 
        place: "Mexico City", 
        country: "Mexico", 
        location: {
            lat: '34',
            lng: '38'
        },
        temperature: '43 Degree Celsius'
    },
    { 
        place: "London", 
        country: "England", 
        location: {
            lat: '57',
            lng: '34'
        },
        temperature: '26 Degree Celsius'
    }
]


/*
Note: Make sure not to mutate the array in all of the following questions.


Q1. Get all latitude and longitude of all the places in the following manner.
[{place: (lat, long)}, ...]

Q2. Sort data based on temperature (Low temperature first).

Q3.Rearrange data in the following format
[{ country: { place: { location: {lat, lng }, temperature }}}, ...]

Q4. Change temperature of SouthAfrica "Pretoria" to "49 Degree Celsius".

Q5. Add a new Object in the fourth postiion.
{ 
        place: "Bangalore", 
        country: "India", 
        location: {
            lat: '84',
            lng: '47'
        },
        temperature: '29 Degree Celsius'
 }

 Q6. Delete the third element in the array .
 Q7. Swap elements at position 2 and second last.
*/




// Question 1:


let result = data.map((ele) =>{
    return {place: ele.place,location: ele.location};

});
console.log(result);


// Question 2:

let sortTemparature = data.sort((a, b) =>{
    return  parseInt(a.temperature) - parseInt(b.temperature);
});
console.log(sortTemparature);

// Question 3:


let reArrangeData = data.reduce((acc, curr) =>{
    let obj = {};
    const {country, place, location} = curr;
    obj = {[country]: {
        [place]:{location}
    }}
    acc.push(obj);
    return acc;
},[]);

console.log(reArrangeData);



// Question 4:

let changeTemp = data.map((ele) =>{

    if(ele.place === "Pretoria"){
        ele.temperature = "49 Degree Celsius";
        return ele;
    }
    return ele;

});
console.log(changeTemp);


// Question 5:

let res = data.reduce((acc, curr) =>{

    acc.push(curr);

    return acc
},[]);

let newArray = { 
    place: "Bangalore", 
    country: "India", 
    location: {
        lat: '84',
        lng: '47'
    },
    temperature: '29 Degree Celsius'
}; 

res.splice(3,0,newArray)


console.log(res);


// Question 6:

data.splice(3,1);
console.log(data);


// Question 7:

let swapArray = data;

[swapArray[1], swapArray[3]] = [swapArray[3], swapArray[1]];
console.log(swapArray);